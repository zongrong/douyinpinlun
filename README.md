# uniapp-高仿抖音短视频评论模版

## 插件由来：很多短视频项目都需要评论，但是官方的模版并没有给予很多需要的功能。现在的短视频评论基本都需要表情库、GIF库等功能。评论内容需要丰富的样式等。所以此插件模版便可以以此来丰富你的项目。


# > 1. 引入注意点
```
        （1）common文件夹是必须的，原封不动粘贴复制即可
        
        （2）static/emojis 表情库 需要引入
        
        （3）如果使用抖音评论  static/douyin 文件需要引入
        
        （防止没有图片报错）
        
        （4）uni_modules 文件夹下的文件也需要进行拷贝
        
        （5）up 主，已经改进了 uni-popup.vue 并对此项目进行了专门的适配，所以要注意一下
        
        （6）微信小程序和H5组件 使用的是 douyin-scrollview.vue 【vue】
         
        （7）App端使用的是 douyin-scrollview.nvue 【nvue】
        
```
# H5、小程序、App端均已去除 rich-text 组件。使用自研 文本+表情+GIF 图片解析器。

# 插件自带高级评论框，自动记忆最近表情，同时引入了上千 GIF 图库，极大丰富了评论内容以及评论框样式。

# 模版里面加入了大量注释，几乎每一行都有写注释，方便大家研究。也可以先下载一下体验App，然后发几条评论看看效果，可能会在不同的机器上出现不同的问题，欢迎加群反馈。

# 本插件地址： <https://ext.dcloud.net.cn/plugin?id=7875>

# 演示视频：<https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/2e60a461-9e96-4382-b36c-f77d8c5707c8.MP4>

# H5演示地址：<http://ceshi-7gypkgh8b25fcd1a-1304060577.tcloudbaseapp.com>[可以电脑访问]

# 演示软件（安卓）：<https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/c7424d8b-cfe5-432b-b235-2b35b34dd99b.apk>

## 这里导出了一份JSON数据，方便各位查看 <https://vkceyugu.cdn.bspapp.com/VKCEYUGU-bdb24c6d-8c19-4f80-8e7e-c9c9f037f131/fafbb1ed-72d7-4975-8e53-eb36eb67447c.json>

## 仿抖音评论模版可以配合 另外一个插件进行使用。

# QQ群：953408573

## 仿抖音模版插件地址：<https://ext.dcloud.net.cn/plugin?id=5656>
